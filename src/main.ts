import serverClass from "./server/server.js";
import loginRouter from "./server/routes/loginRouter.js";
import errorHandler from "./middlewares/error.js";
import mainRouter from "./server/routes/mainRouter.js";
import cors from "cors";
import * as dotenv from "dotenv";
dotenv.config();
const server = new serverClass({name:"Task Manager",routers:[loginRouter,mainRouter],errorHandler,middlewares:[cors({
    origin: true,
    credentials: true,
    exposedHeaders:["Authorization"]
})]});

server.listen(3000);