import httpcode from "./httpcodes.js";

export abstract class PrintableError extends Error{
    public httpcode;
    constructor(msg:string,httpcode:number){
        super(msg)
        this.httpcode = httpcode;
        this.stack = "";
        console.log({msg,httpcode});
    }
}

export class BadRequest extends PrintableError{
    constructor(msg:string){
        super(msg,httpcode.BadRequest);
    }
}

export class Unauthorized extends PrintableError{
    constructor(msg="Invalid Password"){
        super(msg,httpcode.Unauthorized);
    }
}

export class Forbidden extends PrintableError{
    constructor(msg="Forbidden"){
        super(msg,httpcode.Forbidden);
    }
}