import { NextFunction, Request, Response } from "express";
import { PrintableError } from "../interfaces/error.js";
import type { ErrorRequestHandler } from "express";

let errorHandler:ErrorRequestHandler = (err: Error, _req: Request, res: Response, _next: NextFunction) => {
  console.error(err);
  if(err instanceof PrintableError){
    res.status(err.httpcode).send({errors: [{ message: err.message}]});
  }else{
    console.log(err.message);
    res.status(500).send({ errors: [{ message: "Something went wrong" }] });
  }
};

export default errorHandler;