import type { RequestHandler } from "express";
import jwt from "jsonwebtoken";
import { Forbidden, PrintableError, Unauthorized } from "../interfaces/error.js";
const validadorToken:RequestHandler = (req,_res,next)=>{
    let auth = req.headers.authorization;
    if(!auth){
        throw new Unauthorized();
    }
    let token = auth.split(" ")[1];
    try{
        let jtoken = jwt.verify(token,process.env.PRIVATE_KEY as string) as {password:string,hash:string};
        if(jtoken.password != process.env.PASSWORD){
            throw new Unauthorized("Token was made with an old password, login again");
        }
        next();
    }catch(err){
        if(err instanceof PrintableError){
            throw err;
        }
        throw new Forbidden();
    }
  
}

export default validadorToken;