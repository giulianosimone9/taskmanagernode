import { ErrorRequestHandler, RequestHandler, Router } from "express";
import express from "express";
import bodyParser from "body-parser";
export default class server{
    private app;
    public name;
    private port;
    constructor({name,routers: routers, errorHandler}:{name:string,routers:Router[],errorHandler:ErrorRequestHandler,middlewares?:[RequestHandler]}){
        this.app = express();
        this.name = name;
        this.port = 0;
        this.app.use(bodyParser.urlencoded({
            extended: true
          }));
        this.app.use(bodyParser.json());
        routers.forEach(router => {
            this.app.use(router);
        });
        this.app.use(errorHandler);
    }
    listen(port:number){
        this.port = port;
        this.app.listen(this.port,()=>{
            console.log(`Server ${this.name} running on port ${this.port}`)
        })
    }
 
}