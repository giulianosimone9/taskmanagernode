import type { Request, Response } from "express";
import { Router } from "express";
import psList from "ps-list";
import { BadRequest } from "../../interfaces/error.js";
import validadorToken from "../../middlewares/validadorToken.js";
import os from "os";
let mainRouter = Router();

mainRouter.use(validadorToken);

mainRouter.get("/manager/process",async (_req:Request,res:Response)=>{
    res.json({process: (await psList()),memory:{free:os.freemem(),total:os.totalmem()}});
})

mainRouter.post("/manager/kill",(req:Request,res:Response)=>{
    let {pid} = req.body;
    let pidNumber = parseInt(pid);
    if(isNaN(pidNumber)){
        throw new BadRequest("pid must be a number");
    }
    process.kill(pidNumber);
    res.json({done:true});
})

export default mainRouter;
