import type { Request, Response } from "express";
import { Router } from "express";
import { BadRequest, Unauthorized } from "../../interfaces/error.js";
import jwt from "jsonwebtoken";
import * as crypto from 'crypto';
let loginRouter = Router();

loginRouter.post("/login", (req:Request,res:Response)=>{
    let {password}:{password:string} = req.body;
    if(!password) throw new BadRequest('Invalid required field "password"');
    if(password != process.env.PASSWORD) throw new Unauthorized();
    let token = jwt.sign({password,hash:crypto.randomBytes(32).toString("hex")}, process.env.PRIVATE_KEY as string, {expiresIn:'31d'})
    res.json({token});
})



export default loginRouter;