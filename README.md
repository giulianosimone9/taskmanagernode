
# taskManager Node

## Task Manager Backend for Nodejs Made with Typescript, Express.js and JSONWebToken 

### Configuration
Run `npm install`and complete the .env file
```
PASSWORD  =  "Password For login endpoint"
PRIVATE_KEY  =  "Private Key for encrypt jwt"
```
### Compile and run
Just use the `npm run serve` and let the code do the rest

### Frontend
If you want a frontend for the API, check [this proyect](https://gitlab.com/giulianosimone9/taskmanager-fronted)
